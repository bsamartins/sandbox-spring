# oauth

## Concepts
```
There are four different roles within OAuth2 we need to consider:

Resource Owner — an entity that is able to grant access to its protected resources
Authorization Server — grants access tokens to Clients after successfully authenticating Resource Owners and obtaining their authorization
Resource Server — a component that requires an access token to allow, or at least consider, access to its resources
Client — an entity that is capable of obtaining access tokens from authorization servers 
```
More details
https://www.baeldung.com/spring-security-oauth2-enable-resource-server-vs-enable-oauth2-sso

## Getting tokens

```
curl noone:noonewilleverguess@localhost:9001/oauth/token -dgrant_type=client_credentials -dscope=any
```

```
curl noone:noonewilleverguess@localhost:8080/oauth/token -dgrant_type=password -dscope=any -dusername=user1 -dpassword=password
```

