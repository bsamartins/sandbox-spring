subprojects {
    apply(plugin = "org.springframework.boot")
    dependencies {
        implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
        implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
        implementation("org.springframework.boot:spring-boot-starter-web")
        implementation("org.springframework.boot:spring-boot-starter-security")
        implementation("org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.2.3.RELEASE")
    }
}
