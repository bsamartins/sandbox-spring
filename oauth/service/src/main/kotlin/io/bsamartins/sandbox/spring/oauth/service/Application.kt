package io.bsamartins.sandbox.spring.oauth.service

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal


@SpringBootApplication
@EnableResourceServer
@RestController
class Application {

    @GetMapping("/hello")
    fun hello() = "Hello World"

    @GetMapping("/user")
    fun user(principal: Principal): Principal {
        return principal
    }
}

fun main(vararg args: String) {
    SpringApplication(Application::class.java).run(*args)
}
